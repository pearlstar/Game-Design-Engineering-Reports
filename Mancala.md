## Mancala

### Game Strategies

We analysed and got to know some basic strategies which help in winning the game. Refer to the image 'mancala.jpg'. We will discuss with respect to player A. Player B is opponent.

![mancala](/home/pearlstud/Documents/Game_Design_and_Engineering/Game-Design-Engineering-Reports/mancala.jpg)

- First move : If each pit has 4 stones, then A should play A4, which will end at A's mancala and A will get one more chance. In second chance, A should play A6 which has 5 stones. Thus A wll create situation such that  B can't get two moves in next turn.
- A will try to keep more stones on his part. So if B gets empty then A can take all his stones. This strategy should be practiced with some caution.
- Looping : If A want to capture the stones from B6 and he has much number of stones in A6 and empty pit in A1, then he can play move with A6. If it ends on empty A1 due to large number of stones then A can keep all stones from B6.
- Rushing : There are some configurations in which A ends up in his mancala many times thus getting more and more turns. For example, A5 : 2 and A6 : 1. In this situation, A's moves A6, A5, A6 will lead him getting all three stones in his mancala.
- Stuffing : If B has lots of opportunities to end up in his empty and hence capturing your stones, then A can use his full pit to fill up B's pit to avoid such situation.


### Game Rule Change

We introduce a new rule called _colonisation_.

* If the last marble that a player drops is in an empty hole of opposite player, that hole becomes a store (Mancala) of the player and _cursed hole_ for the opposite player
* So, its like the player kind of invaded the unguard territory of opposite player and made the territory its new colony.
* A player can have as many colonies of opposite player as possible

### Premise of the rule change

* There was no added advantage of dropping the last marble in an empty hole of opposite player

### Change of Gameplay

- The game now has one more sub-objective, colonise as many holes of opposite players as possible to win the game


#### Player Engagement

* The player will have to be more alert in the game, the rule gives great opportunities for taking lead in the game  

#### Convergence of the game

* The game is as convergent as before, since the marbles are limited, the game will have to end eventually

### Time to complete the game

* It will take lesser time to finish now as the number of holes will decrease and stores will increase.
