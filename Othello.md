## Othello

### Game Analysis And Game Math

- Since Othello is purely strategy based game, we play several times to get some strategies behind it.
- Firstly it was obvious that corner has great importance. Next comes edges and then inside squares.
- We also learned that _mobility_ is important. Mobility is defined as the amount of power with which you can _drive_ the game. More specifically to the game of Othello, it's better to _maximize_ the empty squares touching the opponents pieces. Other way, it's better to minimize the possible available moves to opponent player. It would  serve as a very good heuristic for the game.
- We found concept of _parity_ as a part of strategy. If get the last move in each empty region then it would be beneficial for us. Rational player tries to keep the last move available for him in each empty region.
- There are almost $10^{28}$ legal positions in Othello and game tree complexity can go up to $10^{58}$. Thus it's still computationally unsolved problem.
- $8*8$ is still unsolved but for $4*4$ and $6*6$, optimal stretagy leads to the win of second player. For $8*8$, experimental proofs shows that the game will end with a draw, but not yet proven.

### Game Rule Change

We introduced a new rule called _Corner Siege_.

- When a corner is occupied by white and the surrounding all **three** pieces are occupied by black, then the corner piece will be flipped to black.
- A milder version of rule change can be : When a corner is occupied by white and any **two** the surrounding three pieces are occupied by black, then the corner piece will be flipped to black.

### Premise of Rule Change

- It is known that the corner place holds a big importance in othello. Once a player gets the corner place, it cannot be flipped. Also the player can use that corner piece effectively to flip opponents pieces to his side. Thus there is no chance for the other player to recover from the corner loss.
- We wanted to change the rule such that even after losing the corner, a player has a little chance/strategy to get it back.

### Change in Game Play

- Earlier occupying the corners held great importance in the game play and once occupied, it was immutable. But now, it is possible to dethrone opposite player from the corner. Therefore the game became more flexible and interesting.
- Earlier a player's motive was confined to only occupying the corners but now it is extended to protect from _Corner Siege_.
- Chances of _Corner Siege_ with three pieces
  - If three pieces are required for _Corner Siege_ then it's quite unlikely that a player will succeed in achieving it. 
  - If a player occupies the corner via edge, then it's impossible for the opponent to flip the edged piece near that corner. Hence there will be no chance for _Corner Siege_.
  - If a player occupies the corner via diagonal, then the opponent can occupy the edged blocks near that corner, flip the diagonal piece near corner and hence achieving _Corner Siege_.
- Chances of _Corner Siege_ with two pieces
  - If any two of the three neighbours of the corner is required for _Corner Siege_, then the chances of achieving _Corner Siege_ is increased. Hence, the game becomes interesting.



#### Player Engagement

- Earlier once a player loses some corners early in the game then he was almost destined to lose the game. But now with changed rule, he has a chance to come back with proper strategic thinking.
- Therefore, it makes the game remain interesting till the end.

#### Convergence of the game

- With changed rule, the game takes more time to finish than before. But at each move, once piece is put on the board for sure. Therefore the game will converge to an outcome always.

#### Time to Complete the Game

- Time to finish the game will increase than before because the game will become more flexible with rule change.
- When three pieces are required for _Corner Siege_, the expected completion time of the game remains almost the same.
- When two pieces are required for _Corner Siege_, the expected completion time of the game will increase.