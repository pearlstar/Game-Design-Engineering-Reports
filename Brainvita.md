## Brainvita

### Game Rule Change

We introduce a new rule called _Savior Marble_

* Every player will have exactly one chance to add one marble into the game wherever he wishes

### Premise of Rule Change

* We wanted to give the player some way to improve his result
* Often we come across cases where we wish, if only we had one more marble in that position we would have won, so we attempted to provide the solution through this rule change  

## Change in Game Play

* The game became less competitive than before i.e. easier to score now.
* The most beneficial way to use this new rule is to use it at the end.


## Game Math

### Normal Game

- We ran $100000$ random simulations ([Code File Link](https://gitlab.com/pearlstar/Game-Design-Engineering-Reports)). At each move, a random move from available moves is chosen to play. As usual game ends when there is no move available. We denote the number of remaining marbles as the score of that game. The number of games out of total $100000$ games, which ended with perticular score values are as follows. $ith$ element is the number of games which ended with score $i$ with 1-based indexing.

  [0, 10, 283, 2987, 7803, 16348, 24620, 20186, 15077, 7625, 2564, 1029, 432, 147, 62, 14, 27, 7, 7, 0, 8, 2, 0, 0, 0, 762, 0, 0, 0, 0, 0, 0]

- Expected score in random simulation is : $7.658$

- One surprising observation was score of $26$. Total $762$ of $100000$ games ended with score $26$ which was highest score in our simulation.

  - If we denote the board as $7*7$ grid then the moves which can lead to this highest score is : (5, 3) -> (3, 3), (2, 3) -> (4, 3), (0, 3) -> (2, 3), (3, 5) -> (3, 3), (3, 2) -> (3, 4), (3, 0) -> (3, 2)
  - The board configuration before last move is shown in `brainvita_worst.png`.

  ![brainvita_worst](/home/pearlstud/Documents/Game_Design_and_Engineering/Game-Design-Engineering-Reports/brainvita_worst.png)

### Game With Changed Rule

- For rule change, we ran random simulations. Whenever the player gets out of moves, we selected any random empty place which is a neighbour place to a filled place (to ensure that the game continues). The results are as follows. $ith$ element is the number of games which ended with score $i$ with 1-based indexing.

  [0, 78, 1067, 5482, 12906, 21473, 23817, 17540, 10669, 4584, 1556, 434, 133, 62, 14, 6, 5, 4, 4, 2, 2, 1, 4, 0, 28, 0, 129, 0, 0, 0, 0, 0]

- Expected score is : $6.9739$ which is less than normal game as expected.

- Again surprising observation is score of $27$. Total $129$ games ended with score $27$ which is highest. This can happen when the game goes like the worst game in normal brainvita and the savior marble is added such that no further game is possible.

  ​


#### Player Engagement

* Novice players will take more interest in the game after the rule change because they will find it easier to win.
* Expert players will tend to lose interest in the game after the introduction of this new rule.

#### Convergence of the game

-  The game is still convergent because, no. of marbles are limited on the  board and only 1 (finite) marble is added at any instance of the game.

### Time to Complete the Game

-  It takes slightly more time to end the game than before because where the player would have already ended the game in original version, here in our version, the player will have the chance to introduce one new marble and carry forward the game.
